<?php

/**
 * @file
 * Defines the inline entity form controller for ECK Entities.
 */

class EckInlineEntityFormController extends EntityInlineEntityFormController {

  /**
   * Overrides EntityInlineEntityFormController::labels().
   */
  public function labels() {
    $entity_info = entity_get_info($this->entityType);
    $labels = array(
      'singular' => $entity_info['label'],
      'plural' => $entity_info['label'],
    );
    return $labels;
  }

  /**
   * Overrides EntityInlineEntityFormController::entityForm().
   */
  public function entityForm($entity_form, &$form_state) {
    $entity = $entity_form['#entity'];

    $entity_form['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Title'),
      '#required' => TRUE,
      '#default_value' => isset($entity->title) ? $entity->title : '',
      '#maxlength' => 255,
      '#weight' => -5,
    );

    field_attach_form($entity->type, $entity, $entity_form, $form_state, LANGUAGE_NONE);
    return $entity_form;
  }
}